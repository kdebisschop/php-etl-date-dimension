# Date Dimension

This repository contains some sample code for using [PHP-ETL](https://github.com/leomarquine/php-etl)
to construct a Date Dimension table for a data warehouse.

The steps to make a similar project for your own warehouse are as follows:

## The Project
 
Create a working directory for your project and require PHP-ETL:

```composer require marquine/php-etl```

This work is in a public fork of the PHP-ETL project. There is a pull request to include this new Extractor, but
since it has not yet been reviewed or accepted, we need to use that fork instead of the upstream repository. Until
this work has been adopted by the primary maintainer, edit composer.json adding this:

```
    "repositories":
    [
        {
            "type": "vcs",
            "url": "https://github.com/kdebisschop/php-etl"
        }
    ],
    "minimum-stability": "dev",
    "prefer-stable": true,
```

and update the require specification to

```"marquine/php-etl": "dev-date-dimension-extractor"```

## The Database

By definition the Loader step will require a place to store the result. I have used an SQLite database to provide a quick endpoint for the ETL. The DDL for the schema used is as follows:

```
CREATE TABLE IF NOT EXISTS DimDate
(
	DateKey INT NOT NULL PRIMARY KEY,
	DateFullName VARCHAR(50) NOT NULL,
	DateFull DATE NOT NULL,
	Year INT NOT NULL,
	Quarter INT NOT NULL,
	QuarterName VARCHAR(50) NOT NULL,
	QuarterKey INT NOT NULL,
	Month INT NOT NULL,
	MonthKey INT NOT NULL,
	MonthName VARCHAR(50) NOT NULL,
	DayOfMonth INT NOT NULL,
	NumberOfDaysInTheMonth INT NOT NULL,
	DayOfYear INT NOT NULL,
	WeekOfYear INT NOT NULL,
	WeekOfYearKey INT NOT NULL,
	ISOWeek INT NOT NULL,
	ISOWeekKey INT NOT NULL,
	WeekDay INT NOT NULL,
	WeekDayName VARCHAR(50) NOT NULL,
	FiscalYear INT NULL,
	FiscalQuarter INT NULL,
	FiscalQuarterKey INT NULL,
	FiscalMonth INT NULL,
	FiscalMonthKey INT NULL,
	IsWorkDayKey INT NOT NULL,
	IsWorkDayDescription VARCHAR(50) NULL,
	IsPublicHolidayKey INT NULL,
	IsPublicHolidayDescription VARCHAR(50) NULL
);
```

## The code

```$xslt
<?php
require_once __DIR__ . '/vendor/autoload.php';

use Marquine\Etl\Etl;

Etl::service('db')->addConnection([
    'driver' => 'sqlite',
    'database' => __DIR__ . '/warehouse.sqlite',
]);

$sql = 'CREATE TABLE IF NOT EXISTS DimDate (...)';
$statement = Etl::service('db')->pdo('default')->query($sql);
$statement->execute();

$etl = new Etl();
$etl->extract('date_dimension', 'date_dimension', [])
    ->load('insert_update', 'DimDate', ['key' => ['DateKey']])
    ->run();
```