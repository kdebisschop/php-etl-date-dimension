<?php
// Integrate with composer.
require_once __DIR__ . '/vendor/autoload.php';

use Marquine\Etl\Etl;

// Define the database connection by providing a configuration array to the 'db'
// service of the ETL singleton.
Etl::service('db')->addConnection([
    'driver' => 'sqlite',
    'database' => __DIR__ . '/warehouse.sqlite',
]);

$sql = <<<SQL
CREATE TABLE IF NOT EXISTS DimDate
(
	DateKey INT NOT NULL PRIMARY KEY,
	DateFullName VARCHAR(50) NOT NULL,
	DateFull DATE NOT NULL,
	Year INT NOT NULL,
	Quarter INT NOT NULL,
	QuarterName VARCHAR(50) NOT NULL,
	QuarterKey INT NOT NULL,
	Month INT NOT NULL,
	MonthKey INT NOT NULL,
	MonthName VARCHAR(50) NOT NULL,
	DayOfMonth INT NOT NULL,
	NumberOfDaysInTheMonth INT NOT NULL,
	DayOfYear INT NOT NULL,
	WeekOfYear INT NOT NULL,
	WeekOfYearKey INT NOT NULL,
	ISOWeek INT NOT NULL,
	ISOWeekKey INT NOT NULL,
	WeekDay INT NOT NULL,
	WeekDayName VARCHAR(50) NOT NULL,
	FiscalYear INT NULL,
	FiscalQuarter INT NULL,
	FiscalQuarterKey INT NULL,
	FiscalMonth INT NULL,
	FiscalMonthKey INT NULL,
	IsWorkDayKey INT NOT NULL,
	IsWorkDayDescription VARCHAR(50) NULL,
	IsPublicHolidayKey INT NULL,
	IsPublicHolidayDescription VARCHAR(50) NULL
);
SQL;

// Use the ETL singleton to get a PDO object and instantiate our
// dimension table on the fly.
$statement = Etl::service('db')->pdo('default')->query($sql);
$statement->execute();

// Get an ETL object. Each set of ETL steps generally gets its own ETL object.
$etl = new Etl();

// Define the extraction and load steps, there is no tranform here. Then run
// the ETL pipeline.
$etl->extract('date_dimension', 'date_dimension', [])
    ->load('insert_update', 'DimDate', ['key' => ['DateKey']])
    ->run();
